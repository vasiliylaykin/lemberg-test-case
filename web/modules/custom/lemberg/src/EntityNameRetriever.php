<?php

namespace Drupal\lemberg;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;

/**
 * EntityNameRetriever service.
 */
class EntityNameRetriever implements EntityNameRetrieverInterface {

  /**
   * Route match interface.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * EntityNameRetriever constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $current_route_match
   *   Current route match.
   */
  public function __construct(RouteMatchInterface $current_route_match) {
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserFromRoute() {
    return $this->currentRouteMatch->getParameter('user');
  }

  /**
   * {@inheritdoc}
   */
  public function getUserName() {
    $name = '';
    $user = $this->getUserFromRoute();
    if ($user instanceof UserInterface) {
      $name = $user->getAccountName();
    }

    return $name;
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeFromRoute() {
    return $this->currentRouteMatch->getParameter('node');
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeName() {
    $name = '';
    $node = $this->getNodeFromRoute();
    if ($node instanceof NodeInterface) {
      $name = $node->getTitle();
    }

    return $name;
  }

}
