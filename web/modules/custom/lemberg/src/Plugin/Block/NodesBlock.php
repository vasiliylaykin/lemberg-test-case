<?php

namespace Drupal\lemberg\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "nodes_block",
 *   admin_label = @Translation("Nodes block"),
 *   category = @Translation("Nodes World"),
 * )
 */
class NodesBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Entity view mode storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface
   */
  protected $entityViewModeStorage;

  /**
   * Node storage.
   *
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;

  /**
   * Node viev builder.
   *
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   */
  protected $nodeViewBuilder;

  /**
   * NodesBlock constructor.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityViewModeStorage = $entity_type_manager->getStorage('entity_view_mode');
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->nodeViewBuilder = $entity_type_manager->getViewBuilder('node');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();
    $count = $config['nodes_count'];
    $display_type = $config['display_type'];
    $nids = $this->nodeStorage
      ->getQuery()
      ->range(0, $count)
      ->sort('created')
      ->execute();
    $render = [];
    if (!empty($nids)) {
      $nodes = $this->nodeStorage->loadMultiple($nids);
      foreach ($nodes as $node) {
        $render[] = $this->nodeViewBuilder->view($node, $display_type);
      }
    }

    return $render;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $view_modes = $this->entityViewModeStorage->loadByProperties([
      'targetEntityType' => 'node',
    ]);
    $config = $this->getConfiguration();

    // Get all content types labels.
    $node_types_options = [];
    foreach ($view_modes as $view_mode) {
      $machine_name = str_replace('node.', '', $view_mode->id());
      $node_types_options[$machine_name] = $view_mode->label();
    }

    // Build form.
    $form['nodes_count'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of nodes'),
      '#default_value' => isset($config['nodes_count']) ? $config['nodes_count'] : 5,
    ];
    $form['display_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Display type'),
      '#options' => $node_types_options,
      '#default_value' => isset($config['display_type']) ? $config['display_type'] : '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['nodes_count'] = $form_state->getValue('nodes_count');
    $this->configuration['display_type'] = $form_state->getValue('display_type');
  }

}
