<?php

namespace Drupal\lemberg;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;

/**
 * EntityNameRetriever service interface.
 */
Interface EntityNameRetrieverInterface {


  /**
   * Fetches user from the current route, if available, returns User Name.
   *
   * @return string
   *   User name.
   */
  public function getUserFromRoute();

  /**
   * Fetches node from the current route, if available, returns Title.
   *
   * @return string
   *   Node title.
   */
  public function getNodeFromRoute();

}
